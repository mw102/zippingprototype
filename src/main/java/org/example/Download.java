package org.example;

import lombok.extern.java.Log;
import okhttp3.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.logging.Logger;

public class Download {

    final private String HTTP_USER_AGENT = "Java/" + System.getProperty("java.version");
    private OkHttpClient httpClient;
    private Logger log;

    public Download() {
        this.httpClient = new OkHttpClient.Builder().followRedirects(true).build();
        this.log = Logger.getLogger(getClass().getName());
    }

    public boolean HTTPGetFile(String url_, String destination_) throws Exception {
        Response response = this.HTTPRequest(HttpUrl.parse(url_).newBuilder(), null);
        byte[] buffer = response.body().byteStream().readAllBytes();

        Files.write(Paths.get(destination_), buffer);
        return true;
    }

    public Response HTTPRequest(HttpUrl.Builder httpUrlBuilder, RequestBody data_) throws IOException {
        String url = httpUrlBuilder.build().toString();
        Request.Builder builder = new Request.Builder().addHeader("User-Agent", HTTP_USER_AGENT).url(url);

        this.log.info("Server::HTTPRequest url(" + url + ")");

        if (data_ != null) {
            builder.post(data_);
        }

        Request request = builder.build();
        Response response = null;

        try {
            response = httpClient.newCall(request).execute();

            return response;
        }
        catch (IOException e) {
            throw new IOException("Unexpected response from HTTP Stack" + e.getMessage());
        }
    }
}
