package org.example;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Main {

    public static final String URL_FILE = "parts.txt";
    private Logger log;

    public Main() {
        this.log = Logger.getLogger(getClass().getName());
    }

    public static void main(String[] args) throws Exception {

        Main main = new Main();
        main.doMain(args);

    }

    private void doMain(String[] args) throws Exception {
        var downloadList = Files.readAllLines(Paths.get(URL_FILE));
        List<byte[]> byteBuckets = new ArrayList<>();

        for (int i = 0; i < downloadList.size(); i++) {
            var url = downloadList.get(i);
            byteBuckets.add(Files.readAllBytes(Paths.get(url)));
        }

        int totalLength = byteBuckets.stream().map(bytes -> bytes.length).reduce(Integer::sum).orElse(0);
        log.info(() -> String.format("Loaded %d parts of total size %d into memory", byteBuckets.size(), totalLength));

        var masterBuffer = merge(byteBuckets);
        String md5FileName = md5(masterBuffer) + ".zip";
        Files.write(Paths.get(md5FileName), masterBuffer);
        log.info(() -> String.format("Recombined parts. File: %s Total bytes: %d", md5FileName, masterBuffer.length));

    }

    public static byte[] merge(List<byte[]> arrays) {
        var count = 0;
        // obtain the total length of the merged array
        for (var array : arrays) {
            count += array.length;
        }

        // copy all the arrays into the new array
        var result = new byte[count];
        int start = 0;
        for (var array : arrays) {
            System.arraycopy(array, 0, result, start, array.length);
            start += array.length;
        }
        return result;
    }

    private boolean combine(FileChannel file, InputStream dataSource, long offset, long desiredLength) throws IOException {
        ReadableByteChannel readableByteChannel = Channels.newChannel(dataSource);

        long totalBytesWritten = 0;
        long bytesWritten = 0;
        long chunkSize = 4 * 1024;
        Thread thisThread = Thread.currentThread();
        do {
            bytesWritten = file.transferFrom(readableByteChannel, offset, chunkSize);
            totalBytesWritten += bytesWritten;

            offset += bytesWritten;
        } while (bytesWritten != 0 && totalBytesWritten < desiredLength && thisThread.isInterrupted() == false);

        dataSource.close();
        return totalBytesWritten >= desiredLength;
    }

    public static String md5(byte[] bytes) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            InputStream is = new ByteArrayInputStream(bytes);
            DigestInputStream dis = new DigestInputStream(is, md);
            byte[] buffer = new byte[8192];
            while (dis.read(buffer) > 0)
                ; // process the entire file
            String data = convertBinaryToHex(md.digest());
            dis.close();
            is.close();
            return data;
        }
        catch (NoSuchAlgorithmException | IOException e) {
            return "";
        }
    }

    public static String convertBinaryToHex(byte[] bytes) {
        StringBuilder hexStringBuilder = new StringBuilder();
        for (byte aByte : bytes) {
            char[] hex = new char[2];
            hex[0] = Character.forDigit((aByte >> 4) & 0xF, 16);
            hex[1] = Character.forDigit((aByte & 0xF), 16);
            hexStringBuilder.append(new String(hex));
        }
        return hexStringBuilder.toString();
    }
}
